% apply masks on the parcelled data, cut into stimulus-related segments

pn.root    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
addpath(genpath([pn.root,'analyses/B4_PLS_preproc2/T_tools/NIFTI_toolbox/']));
addpath(genpath([pn.root,'analyses/B4_PLS_preproc2/T_tools/preprocessing_tools/'])); 

pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/B_parcelData/';

% N = 44 YA
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%% load mask data

pn.Gordon = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/Gordon_Parcels/Parcels_MNI_333.nii';
GordonParcels = S_load_nii_2d([pn.Gordon]);

pn.Shine = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/A_LVs/';

LVData = [];
for indLV = 1:5
    LVData_tmp = S_load_nii_2d([pn.Shine, 'LV',num2str(indLV),'.nii']);
    for indParcel = 1:333
         LVData{indLV}(indParcel,:) = mean(LVData_tmp(GordonParcels == indParcel));
    end
end

%% for each subject, extract parcel data

for indID = 1:numel(IDs)
    for indRun = 1:4
        % load parcel data
        load([pn.out, IDs{indID},'_run-',num2str(indRun),'_GordonParcel.mat'], 'ParcelData')
        % create weighted means of time series from parcels 
        for indLV = 1:5
            LVseries(indID,indRun,indLV,:) = squeeze(mean(LVData{indLV}.*ParcelData,1));
        end
    end
end

figure; imagesc(squeeze(nanmean(nanmean(LVseries,2),1)))

%% segment to stimulus periods

pn.timing = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/B_data/A_regressors/';

for indID = 1:numel(IDs)
    blockCount = zeros(4,1);
    for indRun = 1:4
        % load timing information
        load([pn.timing, IDs{indID}, '_Run',num2str(indRun),'_regressors.mat'], 'Regressors', 'RegressorInfo');
        % encode in datamat
        for indCond = 1:4
            onsets = find(sum(Regressors(:,7:10),2)==indCond); % get onsets
            % encode data for each block            
            for indBlock = 1:numel(onsets)
            % TO DO: concatenate across runs!
                blockCount(indCond) = blockCount(indCond)+1;
                LVseries_byCond(indID,indCond,blockCount(indCond),:,:) = squeeze(LVseries(indID,indRun,:,onsets(indBlock):onsets(indBlock)+16)); % encode 16 additional volumes
            end
        end
    end
end

save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/C_lV_series.mat'], 'LVseries_byCond')

figure; plot(squeeze(nanmean(nanmean(nanmean(LVseries_byCond,3),1),2))')


figure; plot(squeeze(nanmean(nanmean(LVseries_byCond(:,:,:,1,:),3),1))')
figure; plot(squeeze(nanmean(nanmean(LVseries_byCond(:,:,:,1,:),3),1))')


% figure;
% hold on;
% lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,1,:),3),1));
% lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,2,:),3),1));
% lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,3,:),3),1));
% plot(lv1, lv2, '-')
% lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,1,:),3),1));
% lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,2,:),3),1));
% lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,3,:),3),1));
% plot(lv1, lv2, '-')
% lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,1,:),3),1));
% lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,2,:),3),1));
% lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,3,:),3),1));
% plot(lv1, lv2, '-')
% lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,1,:),3),1));
% lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,2,:),3),1));
% lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,3,:),3),1));
% plot(lv1, lv2, '-')
% xlabel('tpc1'); ylabel('tpc2');

figure;
hold on;
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,2,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,3,8:end),3),1));
plot3(lv1, lv2, lv3, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,2,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,3,8:end),3),1));
plot3(lv1, lv2, lv3, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,2,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,3,8:end),3),1));
plot3(lv1, lv2, lv3, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,2,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,3,8:end),3),1));
plot3(lv1, lv2, lv3, '-')
xlabel('tpc1'); ylabel('tpc2'); zlabel('tpc3')

figure;
hold on;
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,2,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,5,8:end),3),1));
plot3(lv3, lv1, lv2, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,2,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,5,8:end),3),1));
plot3(lv3, lv1, lv2, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,2,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,5,8:end),3),1));
plot3(lv3, lv1, lv2, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,2,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,5,8:end),3),1));
plot3(lv3, lv1, lv2, '-')
xlabel('tpc5'); ylabel('tpc1'); zlabel('tpc2')


figure;
hold on;
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,4,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,1,:,5,8:end),3),1));
plot3(lv1, lv2, lv3, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,4,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,2,:,5,8:end),3),1));
plot3(lv1, lv2, lv3, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,4,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,3,:,5,8:end),3),1));
plot3(lv1, lv2, lv3, '-')
lv1 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,1,8:end),3),1));
lv2 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,4,8:end),3),1));
lv3 = squeeze(nanmean(nanmean(LVseries_byCond(:,4,:,5,8:end),3),1));
plot3(lv1, lv2, lv3, '-')
xlabel('tpc1'); ylabel('tpc4'); zlabel('tpc5')

% chages in euclidean distance from load 1

for indID = 1:42
    for indCond = 1:4
        for indTR = 1:17
            curData = squeeze(nanmean(LVseries_byCond(indID,indCond,:,:,indTR),3));
            curData_L1 = squeeze(nanmean(LVseries_byCond(indID,1,:,:,indTR),3));
            eucDist(indID,indCond,indTR) = sqrt(sum((curData-curData_L1).^2, 1));
            %vecnorm(curData_L1-curData, 2,2);
        end
    end
end

figure; plot(squeeze(nanmean(eucDist(:,:,:),1))')
