pn.root    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
addpath(genpath([pn.root,'analyses/B4_PLS_preproc2/T_tools/NIFTI_toolbox/']));
addpath(genpath([pn.root,'analyses/B4_PLS_preproc2/T_tools/preprocessing_tools/'])); 

pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/B_parcelData/';

% N = 44 YA
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%% load mask data

pn.Gordon = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/Gordon_Parcels/Parcels_MNI_333.nii';
GordonParcels = S_load_nii_2d([pn.Gordon]);

%% for each subject, extract parcel data

for indID = 1:numel(IDs)
    for indRun = 1:4
        dataIn = S_load_nii_2d(['/Users/kosciessa/Desktop/beegfs/StateSwitch/WIP/G_GLM/B_data/BOLDin/', ...
            IDs{indID},'_run-',num2str(indRun),'_regressed.nii']);
        for indParcel = 1:333
            ParcelData(indParcel,:) = squeeze(nanmean(dataIn(GordonParcels == indParcel,:),1));
        end
        % save parcel data
        save([pn.out, IDs{indID},'_run-',num2str(indRun),'_GordonParcel.mat'], 'ParcelData')
    end
end
