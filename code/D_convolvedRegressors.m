% convolve task regressor with canonical HRF (cf. Shine, 2019)

% time points associated with each block were convolved with a canonical
% hrf 

pn.root    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
addpath(genpath([pn.root,'analyses/B4_PLS_preproc2/T_tools/NIFTI_toolbox/']));
addpath(genpath([pn.root,'analyses/B4_PLS_preproc2/T_tools/preprocessing_tools/'])); 

pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/B_parcelData/';

% N = 44 YA
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% create canonical HRF
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/G_GLM/T_tools/spm12/')
hrf_forConvolution = spm_hrf(.645);

pn.timing = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/B_data/A_regressors/';

% load regressor files & convolve with HRF
for indID = 1%:numel(IDs)
    blockCount = zeros(4,1);
    for indRun = 1:4
        % load timing information
        load([pn.timing, IDs{indID}, '_Run',num2str(indRun),'_regressors.mat'], 'Regressors', 'RegressorInfo');
        % encode in datamat
        for indCond = 1:4
            onsets = find(sum(Regressors(:,7:10),2)==indCond); % get onsets
            regressors = sum(Regressors(:,7:10),2); regressors(regressors>0) = 1;
            % create a 5 volume boxcar
            reg_locs = find(regressors==1); reg_locs = reshape((reg_locs + [1:5])',[],1);
            regressors(reg_locs) = 1;
            regressors_conv = conv(hrf_forConvolution, regressors);
            %figure; plot(regressors_conv)
            % encode data for each block            
            for indBlock = 1:numel(onsets)
            % TO DO: concatenate across runs!
                blockCount(indCond) = blockCount(indCond)+1;
                regByAttribute(indID,indCond,blockCount(indCond),:) = regressors_conv(onsets(indBlock):onsets(indBlock)+16); % encode 16 additional volumes
            end
        end
    end
end

figure; hold on;
plot(squeeze(nanmean(nanmean(nanmean(regByAttribute(1,1,:,:),3),2),1)))

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/D_convolvedRegressor.mat', 'regByAttribute')
