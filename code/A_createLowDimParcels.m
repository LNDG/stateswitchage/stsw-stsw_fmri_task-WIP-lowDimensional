pn.root    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
addpath(genpath([pn.root,'analyses/B4_PLS_preproc2/T_tools/NIFTI_toolbox/']));
addpath(genpath([pn.root,'analyses/B4_PLS_preproc2/T_tools/preprocessing_tools/'])); 

pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/A_LVs/';

%% load Gordon parcellation

pn.Gordon = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/Gordon_Parcels/Parcels_MNI_333.nii';

GordonParcels = load_nii([pn.Gordon]);


%% create 5 low-dimensional maps

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/state_space-master/eigenvec.mat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/J_lowDimensional/B_data/state_space-master/eigenval.mat')

for indLV = 1:5
    Component_tmp{indLV} = GordonParcels;
    for indRegion = 1:333
        Component_tmp{indLV}.img(GordonParcels.img == indRegion) = eigenvec(indRegion, indLV);
    end
    % export
    save_nii(Component_tmp{indLV},[pn.out, 'LV',num2str(indLV),'.nii'])
end

%% extract Gordon parcels from StateSwitch data

%% multiply by low-dimensional maps